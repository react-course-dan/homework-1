import cx from 'classnames'
import './Button.scss';

const Button = (props) => {
    const {
        type = 'button',
        classNames = '',
        boxView,
        underlineView,
        children,
        onClick = () => {
        },
    } = props

    return (
        <button onClick={onClick}
                className={cx('button', classNames, {'_box': boxView}, {'_box-underline': underlineView})}
                type={type}
        >
            {children}
        </button>
    )
}

export default Button