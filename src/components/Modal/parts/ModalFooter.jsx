import Button from "../../Button/Button"

const ModalFooter = ({firstText, secondaryText, clickAgree, clickClose}) =>{
    return(
        <div className="button-wrapper">
            { firstText && <Button boxView onClick={clickAgree}>{firstText}</Button>  }
            { secondaryText && <Button underlineView onClick={clickClose}>{secondaryText}</Button> }
        </div>
    )
}

export default ModalFooter