import cx from 'classnames'

const ModalContainer = ({children, classNames}) => {
    const isActive = true

    return (
        <div className={cx('modal', classNames, {'active': isActive})}>
            <div className={cx('modal-box', classNames)}>
                {children}
            </div>
        </div>
    )
}

export default ModalContainer