import ModalWrapper from './parts/ModalWrapper'
import ModalContainer from './parts/ModalContainer'
import ModalHeader from './parts/ModalHeader'
import ModalBody from './parts/ModalBody'
import ModalFooter from './parts/ModalFooter'
import ModalClose from './parts/ModalClose'
import './Modal.scss'

const Modal = ({
                   title,
                   desc,
                   img,
                   clickAgree,
                   clickClose,
                   firstText,
                   secondaryText
               }) => {
    return (
        <ModalWrapper closeHandler={clickClose}>
            <ModalContainer>
                <ModalHeader>
                    <ModalClose clickClose={clickClose}/>
                </ModalHeader>
                <ModalBody>
                    <div>{img && img}</div>
                    <h2>{title}</h2>
                    <p>{desc}</p>
                </ModalBody>
                <ModalFooter
                    firstText={firstText}
                    secondaryText={secondaryText} c
                    clickAgree={clickAgree}
                    clickClose={clickClose}
                />
            </ModalContainer>
        </ModalWrapper>
    )
}

export default Modal