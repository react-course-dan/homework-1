const ModalWrapper = ({children, closeHandler}) => {
    const handleClick = (e) => {
        if (e.target.classList.contains('modal-wrapper')) {
            closeHandler()
        }
    }

    return (
        <div onClick={handleClick} className="modal-wrapper">{children}</div>
    )
}

export default ModalWrapper