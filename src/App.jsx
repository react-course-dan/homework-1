import './App.css'
import React, {useState} from 'react';
import Button from './components/Button/Button.jsx';
import Modal from './components/Modal/Modal.jsx';

function App() {
    const [isModalOpen, setIsModalOpen] = useState(false);

    const openModal = (content) => {
        setModalContent(content);
        setIsModalOpen(true);
    };

    const closeModal = () => {
        setIsModalOpen(false);
    };

    const [modalContent, setModalContent] = useState({
        title: '',
        desc: '',
        firstText: '',
        secondaryText: ''
    });

    const modalText = () => {
        openModal({
            title: 'Add Product “NAME”',
            desc: 'Description for you product',
            firstText: 'Add to favorite',
        });
    };

    const modalImage = () => {
        openModal({
            title: 'Product delete!',
            image: <img src="https://picsum.photos/seed/picsum/200/300" alt="Random"/>,
            desc: 'By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted.',
            firstText: 'No, Cancel',
            secondaryText: 'Yes, Delete'
        });
    };

    return (
        <>
            <div className="wrapper">
                <Button boxView onClick={modalText}>Open first modal</Button>
                <Button underlineView onClick={modalImage}>Open second modal</Button>
                {isModalOpen && (
                    <Modal
                        title={modalContent.title}
                        desc={modalContent.desc}
                        img={modalContent.image}
                        clickClose={closeModal}
                        clickAgree={closeModal}
                        firstText={modalContent.firstText}
                        secondaryText={modalContent.secondaryText}
                    />
                )}
            </div>
        </>
    )
}

export default App
